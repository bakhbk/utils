import 'package:flutter/foundation.dart';

import 'config.dart';

void log(Object? object) {
  if (UtilsConfig.showLog) debugPrint(object.toString());
}
