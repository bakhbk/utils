import 'package:async/async.dart';

extension FutureFunctionExt<T> on Future<T> Function() {
  static final _cacheMap = <Object, AsyncCache<T>>{};

  Future<T> byLazy([Object? key]) {
    final asyncCache = _cacheMap[key ?? T] ??= AsyncCache<T>.ephemeral();
    return asyncCache.fetch(this);
  }

  static void clearCacheMap() {
    for (final e in _cacheMap.values) {
      e.invalidate();
    }
    _cacheMap.clear();
  }

  static void clearByKey(Object key) {
    _cacheMap[key]?.invalidate();
    _cacheMap.remove(key);
  }
}
