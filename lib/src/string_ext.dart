import 'double_utils.dart';
import 'regex.dart';

extension NullableStringExt on String? {
  bool isEqualIgnoreCase(String value) {
    final s = this;
    if (s == null) return false;
    return s.isEqualIgnoreCase(value);
  }

  String get toStr => this ?? '';
}

extension StringExt on String {
  bool isEqualIgnoreCase(String value) {
    return toLowerCase() == value.toLowerCase();
  }

  bool containsIgnoreCase(String value) {
    return toLowerCase().contains(value.toLowerCase());
  }

  @Deprecated('Use toStr only on Nullable String Type')
  String get toStr => this;

  String toUnderscore() {
    if (isEmpty) return '';
    final exp = RegExp('(?<=[a-z])[A-Z]');
    return replaceAllMapped(exp, (Match m) => '_${m.group(0)}')
        .replaceAll(RegExp(r"[^\s\w]"), '_')
        .replaceAll(' ', '_')
        .replaceAll('__', '_')
        .toLowerCase();
  }

  String toDash() {
    if (isEmpty) return '';
    return toUnderscore().replaceAll('_', '-');
  }

  Uri? get toUri => Uri.tryParse(this);

  String capitalize() {
    if (isEmpty) return '';
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  String replace8to7() => replaceAll(RegExp(RegexUtils.regReplace7or8), '+7');

  String get onlyFloatDigits {
    return replaceAll(RegExp('^(-)|[^0-9.,]+'), '').replaceAll(',', '.');
  }

  double get asDouble => DoubleUtils.toDoubleOr0(onlyFloatDigits);

  String get apiPhone => '+$onlyFloatDigits';
}
