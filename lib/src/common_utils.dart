import 'dart:async';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';

import 'log.dart';

sealed class CommonUtils {
  static final _delayedRunMap = <String, Timer>{};
  static final _lazyCreatorMap = {};

  static Future copyToClipboard(String data) async {
    return FlutterClipboard.copy(data).then((_) => log(data));
  }

  static void delayedRun(
    String key,
    VoidCallback call, {
    int mls = 500,
  }) {
    _delayedRunMap[key]?.cancel();
    _delayedRunMap[key] = Timer(Duration(milliseconds: mls), call);
  }

  static Future<String> getTextFromClipboard() => FlutterClipboard.paste();

  static T lazyCreator<K, T>({
    required dynamic Function() key,
    required T Function() instance,
  }) {
    final aKey = key();
    var value = _lazyCreatorMap[aKey];
    if (value is T) return value;
    value = instance();
    _lazyCreatorMap[aKey] = value;
    return value;
  }

  static void runOnReady(FutureOr Function() call) {
    WidgetsBinding.instance.addPostFrameCallback((_) => call());
  }
}
