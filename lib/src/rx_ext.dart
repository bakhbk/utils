import 'package:rxdart/rxdart.dart';

extension BehaviorSubjectExt<T> on BehaviorSubject<T> {
  static final Map _previousEvent = {};

  Stream<T> skipByKey(key) {
    _previousEvent[key] = valueOrNull;
    return skipWhile((event) {
      final previousEvent = _previousEvent[key];
      if (event != previousEvent) {
        _previousEvent[key] = event;
        return false;
      }
      return true;
    }).distinct();
  }
}

extension StreamExt<T> on Stream<T> {
  static final Map _previousEvent = {};

  Stream<T> skipByKey(key) {
    return skipWhile((event) {
      final previousEvent = _previousEvent[key];
      if (event != previousEvent) {
        _previousEvent[key] = event;
        return false;
      }
      return true;
    }).distinct();
  }
}
