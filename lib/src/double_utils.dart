import 'string_ext.dart';

sealed class DoubleUtils {
  static double toDoubleOr0(Object? source) {
    try {
      if (source is double) return source;
      if (source is num) return source.toDouble();
      final value = source?.toString().onlyFloatDigits ?? "0";
      return double.tryParse(value) ?? 0;
    } catch (_) {
      return 0;
    }
  }

  static double? toDoubleOrNull(Object? source) {
    try {
      if (source is double) return source;
      if (source is num) return source.toDouble();
      return double.tryParse(source.toString());
    } catch (_) {
      return null;
    }
  }
}
