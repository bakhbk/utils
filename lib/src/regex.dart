import 'package:flutter/services.dart';

sealed class RegexUtils {
  static const _allowedChars = '[- a-zA-Zа-яА-Яё]+';
  static const allowedCharsFormatters = [_CInputFormatter(_allowedChars)];

  /// Regex для замены в телефонном номере +7|7 или +8|8 на требуемое значение -> +7 или 8
  static const regReplace7or8 = '^([+][7]|[7]|[+][8]|[8])';

  static String positiveFloat([int decimals = 2]) {
    return r'^[0-9]{0,12}$|^[0-9]\d{0,12}[.,]\d{0,' '$decimals}';
  }
}

class _CInputFormatter implements TextInputFormatter {
  final String _regEx;

  const _CInputFormatter(this._regEx);

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    final regEx = RegExp(_regEx);
    final String newString = regEx.stringMatch(newValue.text) ?? "";
    return newString == newValue.text ? newValue : oldValue;
  }
}
